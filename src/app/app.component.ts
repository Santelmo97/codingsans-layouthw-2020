import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  regForm: FormGroup;
  emailRegx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
  constructor(
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) {}
  showSpinner = false;
  ngOnInit() {
    this.regForm = this.formBuilder.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(8)]),
      ],
      address: [''],
      postcode: [''],
    });
  }
  spin() {
    this.showSpinner = true;
    setTimeout(() => {
      this.showSpinner = false;
      this.openSnackBar('Registration successful, Yay!🎉', 'Close');
    }, 4000);
  }
  openSnackBar(msg: string, act: string) {
    this._snackBar.open(msg, act, {
      duration: 2000,
    });
  }
  submit() {
    if (!this.regForm.valid) {
      return;
    }
    this.spin();
    console.log(this.regForm.value);
  }
}
